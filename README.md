# Digimon API Service

## Summary

This Microservice calls the linked API Below to produce a number of outputs
https://digimon-api.vercel.app

## OpenAPI Documentation

OpenAPI Documentation, which details all the endpoints, can be found in the docs folder.

## Example Requests

### List All Digimon Endpoint

The below is an example request url to send to list all available digimon

```
http://localhost:2323/digimon/list
```

### Find Digimon By Name

The below is an example request url to send to list all available digimon

```
http://localhost:2323/digimon/name/agumon
```

### Find Digimon In List

The Below Curl Command can be used to send a request in to process a list of digimon:

```
curl -X POST http://localhost:2323/digimon/findAll
   -H "Content-Type: application/json"
   -d '[{"name": "agumon"}]'  
```

## CI Jobs


### Code Quality

These Jobs run a Code Quality check to ensure that the code is of a high level.

### Test Coverage

These Jobs are there to run Jacoco and then visualize the test coverage 

Part of this process will run the Maven Tests as well

https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html 
