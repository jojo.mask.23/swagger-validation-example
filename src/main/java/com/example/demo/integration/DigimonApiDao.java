package com.example.demo.integration;

import com.example.demo.exceptions.ApiInvalidRequestException;
import com.example.demo.exceptions.ServiceUnavailableException;
import com.example.demo.exceptions.UnexpectedBehaviourException;
import com.example.demo.model.backend.BackendDigimonModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class DigimonApiDao {

    private static final String DIGIMON_BY_NAME_URL = "https://digimon-api.vercel.app/api/digimon/name/%s";
    private static final String LIST_DIGIMON_URL = "https://digimon-api.vercel.app/api/digimon";

    public List<BackendDigimonModel> listAllDigimon() {
        RestTemplate restTemplate = new RestTemplate();
        log.debug("Attempting to Find Digimon");
        try {
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(
                    LIST_DIGIMON_URL,
                    String.class
            );
            return new ObjectMapper()
                    .readValue(responseEntity.getBody(), new TypeReference<>() {
                    });
        } catch (HttpClientErrorException e) {
            if (e.getRawStatusCode() == 400) {
                throw new ApiInvalidRequestException("No Data Was Found on the Backend API", e);
            }
            throw new ServiceUnavailableException("Unexpected Error Occurred during the call to Backend API", e);
        } catch (Exception e) {
            throw new UnexpectedBehaviourException("Unexpected Behaviour Occurred during DAO Call", e);
        }
    }

    public BackendDigimonModel findDigimonByName(String name, boolean handleClientError) {
        RestTemplate restTemplate = new RestTemplate();
        log.debug("Attempting to Find Digimon");
        try {
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(
                    String.format(DIGIMON_BY_NAME_URL, name),
                    String.class
            );
            Optional<BackendDigimonModel> digimonModel = Arrays.stream(new ObjectMapper()
                    .readValue(
                            responseEntity.getBody(), BackendDigimonModel[].class
                    )).findFirst();
            if (digimonModel.isPresent()) {
                log.debug("Found Digimon");
                return digimonModel.get();
            }
        } catch (HttpClientErrorException e) {
            if (handleClientError) {
                if (e.getRawStatusCode() == 400) {
                    throw new ApiInvalidRequestException("No Data Was Found on the Backend API", e);
                }
                throw new ServiceUnavailableException("Unexpected Error Occurred during the call to Backend API", e);
            }
        } catch (Exception e) {
            throw new UnexpectedBehaviourException("Unexpected Behaviour Occurred during DAO Call", e);
        }
        return null;
    }
}
