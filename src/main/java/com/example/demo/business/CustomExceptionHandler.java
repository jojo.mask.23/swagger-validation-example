package com.example.demo.business;

import com.example.demo.exceptions.ApiInvalidRequestException;
import com.example.demo.exceptions.BadRequestException;
import com.example.demo.exceptions.ServiceUnavailableException;
import com.example.demo.exceptions.UnexpectedBehaviourException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Slf4j
@Component
@ControllerAdvice
public class CustomExceptionHandler {

    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<CustomExceptionResponse> handleBadRequestException(RuntimeException ex, WebRequest request) {
        CustomExceptionResponse response = buildResponse(request, ex.getClass().getName(), ex.getMessage(), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ApiInvalidRequestException.class)
    public ResponseEntity<CustomExceptionResponse> handleBadRequestException(ApiInvalidRequestException ex, WebRequest request) {
        CustomExceptionResponse response = buildResponse(request, ex.getClass().getName(), ex.getMessage(), HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UnexpectedBehaviourException.class)
    public ResponseEntity<CustomExceptionResponse> handleUnexpectedException(UnexpectedBehaviourException ex, WebRequest request) {
        CustomExceptionResponse response = buildResponse(request, ex.getClass().getName(), ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ServiceUnavailableException.class)
    public ResponseEntity<CustomExceptionResponse> handleServiceUnavilableException(ServiceUnavailableException ex, WebRequest request) {
        CustomExceptionResponse response = buildResponse(request, ex.getClass().getName(), ex.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
        return new ResponseEntity<>(response, HttpStatus.SERVICE_UNAVAILABLE);
    }

    /**
     * Helper Method to build up response
     *
     * @param request       Request to process
     * @param exceptionName Class Name of the exception
     * @param message       Message to describe exception (Optional)
     * @param status        Status to use for value and phrase
     * @return Built CustomExceptionResponse object
     */
    private CustomExceptionResponse buildResponse(WebRequest request, String exceptionName, String message, HttpStatus status) {
        return new CustomExceptionResponse(
                dateFormat.format(new Date()),
                status.value(),
                status.getReasonPhrase(),
                message,
                exceptionName,
                ((ServletWebRequest) request).getRequest().getRequestURI());
    }

    @Getter
    @AllArgsConstructor
    @JsonPropertyOrder({
            "timestamp", "status", "error", "errorDescription", "exception", "path"
    })
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CustomExceptionResponse {

        private final String timestamp;

        private final int status;

        private final String error;

        private final String errorDescription;

        private final String exception;

        private final String path;

    }
}
