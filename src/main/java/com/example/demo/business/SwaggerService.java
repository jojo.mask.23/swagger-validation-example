package com.example.demo.business;

import com.example.demo.exceptions.BadRequestException;
import com.example.demo.exceptions.UnexpectedBehaviourException;
import com.github.bjansen.ssv.SwaggerValidator;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class SwaggerService {

    @Value("${validate.requests:false}")
    private boolean validateRequests;

    @Value("${validate.responses:false}")
    private boolean validateResponses;

    private static final String SWAGGER_PATH = "docs/digimon_spec.yaml";
    private final SwaggerValidator swaggerValidator;

    public SwaggerService() throws IOException {
        swaggerValidator = constructSwaggerValidator();
    }

    private static final Map<String, String> REQUEST_DEFS = Map.of(
            "findAll", "/components/schemas/findMultipleDigimon"
    );


    private static final Map<String, String> RESPONSE_DEFINTIONS = Map.of(
            "listDigimon", "/components/schemas/digimon",
            "findByName", "/components/schemas/digimonByName",
            "findAll", "/components/schemas/digimonSearchResults"
    );


    public void validateRequest(String endpoint, String payload) {
        if (validateRequests) {
            log.info("Validating Request Payload");
            String definition = REQUEST_DEFS.get(endpoint);
            validatePayload(payload, definition);
        }
    }

    public void validateResponse(String endpoint, String payload) {
        if (validateResponses) {
            log.info("Validating Response Payload");
            String definition = RESPONSE_DEFINTIONS.get(endpoint);
            validatePayload(payload, definition);
        }
    }

    private SwaggerValidator constructSwaggerValidator() throws IOException {
        Path path = Paths.get(SWAGGER_PATH);
        String data;
        try (Stream<String> stream = Files.lines(path)) {
            data = stream.collect(Collectors.joining("\n"));
        }
        return SwaggerValidator.forYamlSchema(new StringReader(data));
    }


    private void validatePayload(String payload, String definition) {
        try {
            swaggerValidator.validate(payload, definition);
        } catch (ProcessingException e) {
            throw new BadRequestException("Invalid Payload Received", e);
        } catch (Exception e) {
            throw new UnexpectedBehaviourException("Unexpected Behaviour Occurred During Validation", e);
        }
    }
}
