package com.example.demo.business;

import com.example.demo.exceptions.ApiInvalidRequestException;
import com.example.demo.integration.DigimonApiDao;
import com.example.demo.model.backend.BackendDigimonModel;
import com.example.demo.model.frontend.DigimonModel;
import com.example.demo.model.frontend.DigimonRequestModel;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class DigimonService {

    private final DigimonApiDao digimonApiDao;

    public List<DigimonModel> getListOfDigimon(String correlationId) {
        log.info("CorrelationId: {} - Making call to digimon API to get list", correlationId);
        List<BackendDigimonModel> listOfDigimon = digimonApiDao.listAllDigimon();
        log.info("CorrelationId: {} - Call Successful", correlationId);
        List<DigimonModel> digimonModelList = new ArrayList<>();
        listOfDigimon.forEach(backendDigimonModel -> digimonModelList.add(new DigimonModel(backendDigimonModel, true)));
        return digimonModelList;
    }

    public DigimonModel findDigimonByName(String correlationId, String name) {
        log.info("CorrelationId: {} - Making call to digimon API to find digimon", correlationId);
        BackendDigimonModel backendDigimonModel = digimonApiDao.findDigimonByName(name, true);
        DigimonModel digimonModel = new DigimonModel(backendDigimonModel, true);
        log.info("CorrelationId: {} - Call Successful", correlationId);
        return digimonModel;
    }

    public List<DigimonModel> findDigimonFromList(String correlationId, List<DigimonRequestModel> providedList) {
        List<BackendDigimonModel> backendList = new ArrayList<>();
        providedList.forEach(digimonRequestModel -> {
            log.info("CorrelationId: {} - Making call to digimon API to find digimon {}", correlationId, digimonRequestModel.getName());
            BackendDigimonModel backendDigimonModel = digimonApiDao.findDigimonByName(digimonRequestModel.getName(), false);
            if (backendDigimonModel != null) {
                backendList.add(backendDigimonModel);
            } else {
                log.warn("Could not find Digimon {} on the backend", digimonRequestModel.getName());
            }
        });
        if (backendList.isEmpty()) {
            throw new ApiInvalidRequestException("No Results Were Found with the Names provided");
        }
        List<DigimonModel> digimonModelList = new ArrayList<>();
        backendList.forEach(backendDigimonModel -> {
            DigimonModel digimonModel = new DigimonModel(backendDigimonModel, true);
            digimonModelList.add(digimonModel);
        });
        return digimonModelList;
    }
}
