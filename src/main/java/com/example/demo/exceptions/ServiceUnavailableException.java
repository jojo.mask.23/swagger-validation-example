package com.example.demo.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ServiceUnavailableException extends RuntimeException {

    public ServiceUnavailableException(String message) {
        super(message);
    }

    public ServiceUnavailableException(String message, Throwable cause) {
        super(message);
        log.warn(message, cause);
    }
}
