package com.example.demo.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApiInvalidRequestException extends RuntimeException {

    public ApiInvalidRequestException(String mesage) {
        super(mesage);
    }

    public ApiInvalidRequestException(String message, Throwable cause) {
        super(message);
        log.warn(message, cause);
    }
}
