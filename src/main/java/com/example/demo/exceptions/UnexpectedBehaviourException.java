package com.example.demo.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UnexpectedBehaviourException extends RuntimeException {

    public UnexpectedBehaviourException(String message) {
        super(message);
    }

    public UnexpectedBehaviourException(String message, Throwable cause) {
        super(message, cause);
        log.error(message, cause);
    }
}
