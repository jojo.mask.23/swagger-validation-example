package com.example.demo.presentation;

import com.example.demo.business.DigimonService;
import com.example.demo.business.SwaggerService;
import com.example.demo.model.frontend.DigimonModel;
import com.example.demo.model.frontend.DigimonRequestModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
public class DigimonController {

    private final DigimonService digimonService;

    private final SwaggerService swaggerService;

    @PreAuthorize("permitAll()")
    @ApiOperation(value = "Get List of Digimon", notes = "This API triggers a call to the Digimon API to get a list of digimon")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Acquiring List")})
    @GetMapping("/digimon/list")
    @ResponseStatus(HttpStatus.OK)
    public List<DigimonModel> getListOfDigimon(@RequestHeader(value = "correlationId") String correlationId) {
        return digimonService.getListOfDigimon(correlationId);
    }

    @PreAuthorize("permitAll()")
    @ApiOperation(value = "Get List of Digimon", notes = "This API triggers a call to the Digimon API to try & find a digimon")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Finding Digimon")})
    @GetMapping("/digimon/name/{name}")
    @ResponseStatus(HttpStatus.OK)
    public DigimonModel findDigimonByName(@RequestHeader(value = "correlationId") String correlationId,
                                          @PathVariable(value = "name") String name) {
        return digimonService.findDigimonByName(correlationId, name);
    }

    @PreAuthorize("permitAll()")
    @ApiOperation(value = "File Multiple Digimon Via Payload", notes = "This API triggers a call to the Digimon API to try & find a list of digimon")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Finding Digimon")})
    @PostMapping("/digimon/findAll")
    @ResponseStatus(HttpStatus.OK)
    public Object findMultipleDigimonByFile(@RequestHeader(value = "correlationId") String correlationId,
                                            @RequestBody List<DigimonRequestModel> providedList) throws JsonProcessingException {
        String enddpoint = "findAll";
        swaggerService.validateRequest(enddpoint, new ObjectMapper().writeValueAsString(providedList));
        List<DigimonModel> digimonModelList = digimonService.findDigimonFromList(correlationId, providedList);
        if (digimonModelList.size() == 1) {
            DigimonModel digimonModel = digimonModelList.get(0);
            swaggerService.validateResponse(enddpoint, new ObjectMapper().writeValueAsString(digimonModel));
            return digimonModel;
        }
        swaggerService.validateResponse(enddpoint, new ObjectMapper().writeValueAsString(digimonModelList));
        return digimonModelList;
    }


}
