package com.example.demo.model.backend;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({
        "name", "img", "level"
})
public class BackendDigimonModel {

    private String name;

    private String img;

    private String level;
}
