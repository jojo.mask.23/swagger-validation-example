package com.example.demo.model.frontend;

import com.example.demo.model.backend.BackendDigimonModel;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({
        "name", "ranking"
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DigimonModel {

    private String name;

    private String ranking;

    public DigimonModel(BackendDigimonModel backendDigimonModel, boolean includeRanking) {
        this.name = backendDigimonModel.getName();
        if (includeRanking)
            this.ranking = backendDigimonModel.getLevel();
    }
}
