package com.example.demo.integration.presentation;

import com.example.demo.business.DigimonService;
import com.example.demo.business.SwaggerService;
import com.example.demo.exceptions.ApiInvalidRequestException;
import com.example.demo.exceptions.BadRequestException;
import com.example.demo.exceptions.ServiceUnavailableException;
import com.example.demo.exceptions.UnexpectedBehaviourException;
import com.example.demo.model.backend.BackendDigimonModel;
import com.example.demo.model.frontend.DigimonModel;
import com.example.demo.model.frontend.DigimonRequestModel;
import com.example.demo.utils.SwaggerSchemaValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("Integration")
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@WithMockUser(username = "admin", authorities = {"ADMIN", "USER"})
@DisplayName("All Tests for Digimon Controller")
class DigimonControllerTests {

    private static final String CORRELATION_ID = "CORR_ID";

    private static final String LIST_DIGIMON_ENDPOINT = "/digimon/list";

    private static final String FIND_DIGIMON_ENDPOINT = "/digimon/name/{name}";

    private static final String FIND_ALL_DIGIMON_ENDPOINT = "/digimon/findAll";

    @Autowired
    private WebApplicationContext webApplicationContext;

    private SwaggerSchemaValidator swaggerSchemaValidator;

    @MockBean
    DigimonService digimonService;

    @MockBean
    SwaggerService swaggerService;

    MockMvc mockMvc;

    public static Stream<Arguments> listCallExceptions() {
        return Stream.of(
                Arguments.of(404, new ApiInvalidRequestException("No Data Found")),
                Arguments.of(500, new UnexpectedBehaviourException("Something unexpected Occurred")),
                Arguments.of(503, new ServiceUnavailableException("Backend Call Failed"))
        );
    }

    public static Stream<Arguments> findByNameExceptions() {
        return Stream.of(
                Arguments.of(400, new BadRequestException("Bad Request Received")),
                Arguments.of(404, new ApiInvalidRequestException("No Data Found")),
                Arguments.of(500, new UnexpectedBehaviourException("Something unexpected Occurred")),
                Arguments.of(503, new ServiceUnavailableException("Backend Call Failed"))
        );
    }

    @BeforeEach
    public void setup() {
        reset(digimonService);
        swaggerSchemaValidator = new SwaggerSchemaValidator();
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .alwaysDo(MockMvcResultHandlers.print())
                .build();
    }

    @Test
    void testSuccessfulListCall() throws Exception {
        when(digimonService.getListOfDigimon(CORRELATION_ID)).thenReturn(Collections.singletonList(new DigimonModel("Agumon", "Rookie")));
        MvcResult mvcResult = mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(LIST_DIGIMON_ENDPOINT)
                                .header("correlationId", CORRELATION_ID)
                )
                .andExpect(status().is(200))
                .andReturn();
        swaggerSchemaValidator.validatePayload("listDigimon", 200, mvcResult.getResponse().getContentAsString());
    }

    @ParameterizedTest(name = "#{index} - Test to Validate Body for Exception {0}")
    @MethodSource(value = "listCallExceptions")
    @DisplayName("All Exception Tests for List Digimon Call")
    void testListDigimonExceptionPayloadValidated(int status, Throwable cause) throws Exception {
        when(digimonService.getListOfDigimon(CORRELATION_ID)).thenThrow(cause);
        MvcResult mvcResult = mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(LIST_DIGIMON_ENDPOINT)
                                .header("correlationId", CORRELATION_ID)
                )
                .andExpect(status().is(status))
                .andReturn();
        swaggerSchemaValidator.validatePayload("listDigimon", status, mvcResult.getResponse().getContentAsString());
    }


    @Test
    void testSuccessfulFindDigimonCall() throws Exception {
        String name = "Agumon";
        when(digimonService.findDigimonByName(CORRELATION_ID, name)).thenReturn(new DigimonModel("Agumon", "Rookie"));
        MvcResult mvcResult = mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(FIND_DIGIMON_ENDPOINT, name)
                                .header("correlationId", CORRELATION_ID)
                )
                .andExpect(status().is(200))
                .andReturn();
        swaggerSchemaValidator.validatePayload("findByName", 200, mvcResult.getResponse().getContentAsString());
    }

    @ParameterizedTest(name = "#{index} - Test to Validate Body for Exception {0}")
    @MethodSource(value = "findByNameExceptions")
    @DisplayName("All Exception Tests for Find Digimon By Name Call")
    void testFindByNameExceptionsPayloadValidated(int status, Throwable cause) throws Exception {
        when(digimonService.getListOfDigimon(CORRELATION_ID)).thenThrow(cause);
        MvcResult mvcResult = mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(LIST_DIGIMON_ENDPOINT)
                                .header("correlationId", CORRELATION_ID)
                )
                .andExpect(status().is(status))
                .andReturn();
        swaggerSchemaValidator.validatePayload("findByName", status, mvcResult.getResponse().getContentAsString());
    }

    @Test
    void testSuccessfulFindOfSingleDigimonFromList() throws Exception {
        List<DigimonRequestModel> requestList = Arrays.asList(
                new DigimonRequestModel("Agumon"),
                new DigimonRequestModel("Devimon")
        );
        when(digimonService.findDigimonFromList(any(), any())).thenReturn(List.of(
                new DigimonModel(new BackendDigimonModel("Agumon", null, "Rookie"), true)
        ));
        MvcResult mvcResult = mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(FIND_ALL_DIGIMON_ENDPOINT)
                                .header("correlationId", CORRELATION_ID)
                                .content(new ObjectMapper().writeValueAsString(requestList))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.ranking").exists())
                .andReturn();
        swaggerSchemaValidator.validatePayload("findAll", 200, mvcResult.getResponse().getContentAsString());
    }


}
