package com.example.demo.integration;

import com.example.demo.model.frontend.DigimonModel;
import com.example.demo.utils.SwaggerSchemaValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

@Tag("Integration")
class SwaggerTests {

    private SwaggerSchemaValidator swaggerSchemaValidator;

    @BeforeEach
    public void setup() {
        swaggerSchemaValidator = new SwaggerSchemaValidator();
    }

    @Test
    void testListResponse() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<DigimonModel> digimonModelList = Arrays.asList(
                new DigimonModel("Agumon", "Rookie"),
                new DigimonModel("Devimon", "Champion")
        );
        swaggerSchemaValidator.validatePayload("listDigimon", 200, objectMapper.writeValueAsString(digimonModelList));
    }
}
