package com.example.demo.integration.business;

import com.example.demo.business.DigimonService;
import com.example.demo.integration.DigimonApiDao;
import com.example.demo.model.backend.BackendDigimonModel;
import com.example.demo.model.frontend.DigimonModel;
import com.example.demo.model.frontend.DigimonRequestModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@Tag("Integration")
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DigimonServiceTests {

    private static final String CORR_ID = "CORR_ID";

    private DigimonService digimonService;

    private static final BackendDigimonModel STANDARD_DATA = new BackendDigimonModel("Agumon", "img", "Rookie");

    @MockBean
    DigimonApiDao digimonApiDao;

    @BeforeEach
    public void setup() {
        reset(digimonApiDao);
        digimonService = new DigimonService(digimonApiDao);
    }

    @Test
    void testSuccessfulGetList() {
        when(digimonApiDao.listAllDigimon()).thenReturn(Collections.singletonList(STANDARD_DATA));
        List<DigimonModel> digimonModelList = digimonService.getListOfDigimon(CORR_ID);
        assertFalse(digimonModelList.isEmpty());
        assertEquals(1, digimonModelList.size());
    }

    @Test
    void testSuccessfulFindDigimonByName() {
        when(digimonApiDao.findDigimonByName("Agumon", true)).thenReturn(STANDARD_DATA);
        DigimonModel digimonModel = digimonService.findDigimonByName(CORR_ID, "Agumon");
        assertEquals("Agumon", digimonModel.getName());
        assertEquals("Rookie", digimonModel.getRanking());
    }

    @Test
    void testSuccessfulFindFromListSingular() {
        when(digimonApiDao.findDigimonByName("Agumon", false)).thenReturn(STANDARD_DATA);
        List<DigimonModel> digimonModelList = digimonService.findDigimonFromList(CORR_ID, List.of(new DigimonRequestModel("Agumon")));
        assertFalse(digimonModelList.isEmpty());
        assertEquals(1, digimonModelList.size());
        assertEquals("Agumon", digimonModelList.get(0).getName());
    }

}
