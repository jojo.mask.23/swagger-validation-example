package com.example.demo.utils;

import com.github.bjansen.ssv.SwaggerValidator;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@Slf4j
@NoArgsConstructor
public class SwaggerSchemaValidator {

    private static final String SWAGGER_PATH = "docs/digimon_spec.yaml";

    private final Map<String, Map<Integer, String>> URI_DEFINITIONS = Map.of(
            "listDigimon", Map.of(
                    200, "/components/schemas/digimon",
                    404, "/components/schemas/clientErrorNoDataResponse",
                    500, "/components/schemas/internalServerErrorException",
                    503, "/components/schemas/serviceUnavailableException"),

            "findByName", Map.of(
                    200, "/components/schemas/digimonByName",
                    400, "/components/schemas/clientErrorBadRequest",
                    404, "/components/schemas/clientErrorNoDataResponse",
                    500, "/components/schemas/internalServerErrorException",
                    503, "/components/schemas/serviceUnavailableException"),

            "findAll", Map.of(
                    200, "/components/schemas/digimonSearchResults",
                    400, "/components/schemas/clientErrorBadRequest",
                    404, "/components/schemas/clientErrorNoDataResponse",
                    500, "/components/schemas/internalServerErrorException",
                    503, "/components/schemas/serviceUnavailableException")
    );


    /**
     * Method to validate a response payload for a given uri and status
     *
     * @param uri     URI to act as key
     * @param status  Status to act as secondary key
     * @param payload Payload to validate
     */
    public void validatePayload(String uri, int status, String payload) {
        try {
            SwaggerValidator swaggerValidator = constructSwaggerValidator();
            String definition = URI_DEFINITIONS.get(uri).get(status);
            ProcessingReport report = swaggerValidator.validate(payload, definition);
            assertTrue(report.isSuccess(), report.toString());
        } catch (NullPointerException | ProcessingException | IOException e) {
            log.info(e.getMessage());
            fail(e);
        }
    }

    private SwaggerValidator constructSwaggerValidator() throws IOException {
        Path path = Paths.get(SWAGGER_PATH);
        String data = Files.lines(path).collect(Collectors.joining("\n"));
        return SwaggerValidator.forYamlSchema(new StringReader(data));
    }

}
