package com.example.demo.functional;

import com.example.demo.model.frontend.DigimonRequestModel;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@Tag("functional")
@SpringBootTest(classes = TestApplication.class)
class DigimonControllerTests {

    @Autowired
    private RestTemplate restTemplate;

    private static final String BASE_PATH = "http://localhost:2323/digimon";

    @Test
    void test400ForListEndpoint() throws IOException {
        String url = BASE_PATH.concat("/findAll");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("CorrelationId", UUID.randomUUID().toString());
        HttpEntity<List<DigimonRequestModel>> httpEntity = new HttpEntity<>(
                List.of(new DigimonRequestModel("Agumon123456")),
                httpHeaders
        );
        ResponseEntity<Resource> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Resource.class);
        assertEquals(404, responseEntity.getStatusCodeValue());
        assertNotNull(responseEntity.getBody());
        String actual = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(responseEntity.getBody()).getInputStream()))
                .lines().collect(Collectors.joining("\n"));
        assertTrue(actual.contains("No Results Were Found with the Names provided"));
    }
}
